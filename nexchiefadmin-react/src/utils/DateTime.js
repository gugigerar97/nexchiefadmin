const getLocalDateTime = (type) => {
    let today = new Date();
    if(type === "dateTime") {
        today = today.toISOString().slice(0,10) + " " + today.toLocaleTimeString().slice(0,7);
    }else if(type === "date") {
        today = today.toISOString().slice(0,10);
    }
    return today;
}

export default getLocalDateTime;