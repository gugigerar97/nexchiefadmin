import { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Topnav from "../components/Topnav/Topnav";
import Navbar from "../components/Navbar/Navbar";
import ActionMenu from "../components/ActionMenu/ActionMenu";
import PrincipalsSideContent from "../components/SideContent/PrincipalsSideContent";
import PrincipalFormContent from "../components/FormContent/PrincipalFormContent";
import API from "../services/APIServices";
import DateTime from "../utils/DateTime";
import { validEmail, validFax, validLetterNumber, validLetterSpace, validPhone } from "../utils/Regex";

class Principal extends Component {
    constructor(props){
        super(props);
        this.state={
            id: "",
            principal_id: "",
            principal_name: "",
            address: "",
            city: "",
            email: "",
            phone: "",
            fax: "",
            country: "",
            contactFName: "",
            contactLName: "",
            contactPhone: "",
            contactEmail: "",
            expireDate: "",
            createdAt: DateTime("dateTime"),
            createdBy: this.props.currentUser,
            updateBy: this.props.currentUser,
            updateAt: DateTime("dateTime"),
            error: {},
            principalEdit: {},
            isDisabled: false,
            isEditable: false,
            principalList: [],
        }
        this.baseState = {...this.state};
    }

    componentDidMount() {
        API.getAll("/principal")
            .then(res => {
                if(res.status === 200) {
                    this.setState({principalList: res.data})
                }
            })
            .catch(error => { console.log("Error: ", error); })
    }
    
    refreshStateToEmpty = () => {
        this.setState({
            ...this.baseState,
            principalList: this.state.principalList
        });
    };

    data(type) {
        let json = {
            ...this.state,
            createdAt: type === "create" ? DateTime("dateTime") : this.state.createdAt,
            createdBy:this.state.createdBy,
            updateBy: this.props.currentUser,
            updateAt: DateTime("dateTime"),
        };
        delete json.error;
        delete json.principalEdit;
        delete json.isDisabled;
        delete json.isEditable;
        delete json.principalList
        return json;
    }

    handleChange = (event) => {
        const {name, value} = event.target;
        this.setState((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const isValid = this.isValidate();
        if (isValid) {
            if (this.state.id === "") {
                this.createPrincipal();
            } else {
                this.updatePrincipal();
            }
        }
    };

    handleEdit = (event) => {
        event.preventDefault();
        this.setState({ isDisabled: false, isEditable: true });
    };

    createPrincipal = () => {
        API.create("/principal", this.data("create"))
        .then(res => {
            if(res.data.status === 200){
                alert(res.data.messages);
                window.location.reload();
            }
        })
        .catch(error => { console.log("Error: ", error); })
    }

    updatePrincipal = () => {
        API.update("/principal", this.state.id, this.data("update"))
        .then(res => {
            if(res.data.status === 200){
                alert(res.data.messages);
                window.location.reload();
            }
        })
        .catch(error => { console.log("Error: ", error); })
    }

    getDataById = (id) => {
        const selectedItems = this.state.principalList.find((values) => values.id === id);
        if (selectedItems) {
            this.setState({                
                ...selectedItems,
                isDisabled: true,
                isEditable: true,
                error: {},
                principalEdit: selectedItems,
            });
        }
    };

    deleteData = () => {
        let confirmation = window.confirm(
            "Are you sure you want to delete " + this.state.principal_name + "?"
        );
        if (confirmation === true) {
            API.delete("/principal", this.state.id)
                .then((res) => {
                    if (res.status === 200) {
                        alert("Principal has been successfully deleted!");
                        window.location.reload();
                    }
                })
                .catch(error => { console.log("Error: ", error); })
        }
    };

    cancelEdit = () => {
        let data = this.state.principalEdit;
        this.setState({
            ...data,
            error: {},
            isDisabled: true,
        });
    };

    isValidate = () => {
        let validate = {
            principalIdError: "", 
            principalnameError: "",
            addressError : "",
            cityError : "",
            emailError : "",
            phoneError : "",
            faxError : "",
            countryError : "",
            contactFNameError: "",
            contactLNameError: "",
            contactPhoneError: "",
            contactEmailError : "",
        };
        let id = this.state.principalList.map((data) => data.principal_id);

        if(id.includes(this.state.principal_id) && !this.state.id){
            validate.principalIdError = "This Principal ID has already been taken";
        }
        if(!validLetterNumber.test(this.state.principal_id)){
            validate.principalIdError = "Principal ID Only Can Be Letters & Numbers";
        }
        if (!this.state.principal_name) {
            validate.principalnameError = "*Principal name is required";
        }
        if (!validLetterSpace.test(this.state.principal_name)){
            validate.principalnameError = "*Principal Name Only Can Be Letters And Spaces"
        }
        if (!this.state.address) {
            validate.addressError = "*Address is required";
        }
        if (this.state.address.length > 255) {
            validate.addressError = "*Address Length Can't Be More Than 255";
        }
        if (!this.state.city) {
            validate.cityError = "*City is required";
        } 
        if (!validLetterSpace.test(this.state.city)) {
            validate.cityError = "*City Only Can Be Letters And Spaces";
        }
        if (!this.state.email) {
            validate.emailError = "*Email is required";
        }
        if (!validEmail.test(this.state.email)) {
            validate.emailError = "*Invalid Email Format, ex: username@domain.com";
        }
        if (!this.state.phone) {
            validate.phoneError = "*Phone is required";
        }
        if (!validPhone.test(this.state.phone)) {
            validate.phoneError = "*Invalid Phone Format, ex: +62/62/08xxxxxxxx";
        }
        if (!validFax.test(this.state.fax)) {
            validate.faxError = "*Invalid Fax Format, ex: (0xx)xxxxxxx"
        }
        if (!validLetterSpace.test(this.state.country)) {
            validate.countryError = "*Country Only Can Be Letters And Spaces"
        }
        if (!validLetterSpace.test(this.state.contactFName)) {
            validate.contactFNameError = "*Contact First Name Only Can Be Letters And Spaces"
        }
        if (!validLetterSpace.test(this.state.contactLName)) {
            validate.contactLNameError = "*Contact Last Name Only Can Be Letters And Spaces"
        }
        if (!validPhone.test(this.state.contactPhone)) {
            validate.contactPhoneError = "*Invalid Contact Phone Format, ex: +62/62/08xxxxxxxx";
        }
        if (!validEmail.test(this.state.contactEmail)) {
            validate.contactEmailError = "*Invalid Contact Email Format, ex: username@domain.com";
        }
        if (validate.principalIdError || validate.principalnameError || validate.contactEmailError || 
            validate.cityError || validate.emailError || validate.phoneError || validate.contactLNameError || 
            validate.faxError || validate.countryError || validate.contactFNameError || validate.addressError ||
            validate.contactPhoneError) {
            this.setState({
                error: validate,
            });
            return false;
        }
        return true;
    };

    render(){
        if (this.props.token !== "") {
        return(
            <>
                <Topnav section="PRINCIPAL" user={this.props.currentUser}/>
                <Navbar/>
                <ActionMenu 
                    {...this.state} 
                    delete={this.deleteData} 
                    submitHandler={this.handleSubmit}
                    editHandler={this.handleEdit} 
                    cancelEdit={this.cancelEdit} 
                    refreshForm={this.refreshStateToEmpty}
                />

                <PrincipalsSideContent 
                    getId={this.getDataById} 
                    dataPrincipal={this.state.principalList}
                />
                <PrincipalFormContent 
                    {...this.state}
                    minDate = {DateTime("date")}
                    handleChange={this.handleChange}
                />
            </>
        )
        }else{
            return(
                <Redirect to="/login"/>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        currentUser: state.username,
        token: state.token,
    }
}

export default connect(mapStateToProps)(Principal);