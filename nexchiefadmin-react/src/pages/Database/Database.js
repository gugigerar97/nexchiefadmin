import { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Topnav from "../../components/Topnav/Topnav";
import Navbar from "../../components/Navbar/Navbar";
import "./Database.css";
import API from "../../services/APIServices";

class User extends Component {
    handleClick = () => {
        this.fetchBackup();
    }

    fetchBackup = () => {
        API.getAll("/database/export")
        .then(res => {
            if(res.status === 200){
                alert(res.data);
            }
        })
        .catch(error => { console.log("Error: ", error); })
    }

    render(){
        if (this.props.token !== "") {
        return(
            <>
                <Topnav section="DATABASE" user={this.props.currentUser}/>
                <Navbar/>

                <div className="parent_database">
                    <div className="column_database">
                    <div className="card_database">
                    <h3>Export Database</h3>
                    <p>The Exported file path is located in : <b>'C:/Users/user/Downloads/'</b></p>
                    <button id="export" onClick={this.handleClick}>
                        Backup
                    </button>
                    </div>
                </div>
                
            </div>
            </>
        )
        }else{
            return(
                <Redirect to="/login"/>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.token,
        currentUser: state.username,
    }
}

export default connect(mapStateToProps)(User);