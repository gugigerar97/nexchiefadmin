import { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Topnav from "../components/Topnav/Topnav";
import Navbar from "../components/Navbar/Navbar";
import ActionMenu from "../components/ActionMenu/ActionMenu";
import UserSideContent from "../components/SideContent/UserSideContent";
import UserFormContent from "../components/FormContent/UserFormContent";
import API from "../services/APIServices";
import DateTime from "../utils/DateTime";
import { validEmail, validLetterSpace, validPassword, validPhone, validUserID } from "../utils/Regex";

class Database extends Component {
    constructor(props){
        super(props);
        this.state={
            id: "",
            userId: "",
            fullName: "",
            password: "",
            address: "",
            phone: "",
            email: "",
            status: "",
            principal: {
                id: "",
            },
            distributor: [
                {id:""}
            ],
            disableLogin: false,
            registrationDate: DateTime("dateTime"),
            productValidThru: "",
            createdAt: DateTime("dateTime"),
            createdBy: this.props.currentUser,
            updateBy: this.props.currentUser,
            updateAt: DateTime("dateTime"),
            error: {},
            userEdit: {},
            isDisabled: false,
            isEditable: false,
            userList: [],
            principalList: [],
            distributorList: [],
            distributorOption: []
        }
        this.baseState = {...this.state};
    };

    componentDidMount() {
        API.getAll("/users")
            .then(res => {
                if(res.status === 200){
                    this.setState({userList: res.data})
                }
            })
            .catch(error => { console.log("Error: ", error); })
        API.getAll("/distributor")
            .then(res => {
                if(res.status === 200){
                    this.setState({distributorList: res.data})
                }
            })
            .catch(error => { console.log("Error: ", error); })
        API.getAll("/principal")
            .then(res => {
                if(res.status === 200){
                    this.setState({principalList: res.data})
                }
            })
            .catch(error => { console.log("Error: ", error); })
    };
    
    refreshStateToEmpty = () => {
        this.setState({
            ...this.baseState,
            userList: this.state.userList,
            principalList: this.state.principalList,
            distributorList: this.state.distributorList
        });
    };

    data(type) {
        let distributorEmpty = [];
        this.state.distributorOption.forEach(data => {
            distributorEmpty.push({id: data.id})
        });
        let json = {
            ...this.state,
            principal: {
                id: this.state.principal.id
            },
            distributor: this.state.distributor[0].id !== ""? (this.state.distributor) : (distributorEmpty),
            registrationDate:  type === "create" ? DateTime("dateTime") : this.state.registrationDate,
            createdAt: type === "create" ? DateTime("dateTime") : this.state.createdAt,
            updateBy: this.props.currentUser,
            updateAt: DateTime("dateTime"),
        };
        delete json.error;
        delete json.userEdit;
        delete json.isDisabled;
        delete json.isEditable;
        delete json.userList;
        delete json.distributorList;
        delete json.principalList;
        delete json.distributorOption;
        return json;
    };

    handleChange = (event) => {
        const {name, value} = event.target;
        if(event.target.id === "disableLogin") {
            this.setState((prevState) => ({
                ...prevState,
                disableLogin : event.target.checked
            }))
        }else if(event.target.id === "distributor"){
            let objek = { id: value };
            this.setState((prevState) => ({
                ...prevState,
                distributor : [ objek ]
            }));
        }
        else {
            this.setState((prevState) => ({
                ...prevState,
                [name]: value
            }))
        }
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const isValid = this.isValidate();
        if (isValid) {
            if (this.state.id === "") {
                this.createUser();
            } else {
                this.updateUser();
            }
        }
    };

    handleEdit = (event) => {
        event.preventDefault();
        this.setState({ isDisabled: false, isEditable: true });
    };

    createUser = () => {
        console.log(this.data("create"));
        API.create("/users", this.data("create"))
        .then(res => {
            if(res.data.status === 200){
                alert(res.data.messages);
                window.location.reload();
            }
        })
        .catch(error => { console.log("Error: ", error); })
    };

    updateUser = () => {
        console.log(this.data("update"));
        API.update("/users", this.state.id, this.data("update"))
        .then(res => {
            if(res.data.status === 200){
                alert(res.data.messages);
                window.location.reload();
            }
        })
        .catch(error => { console.log("Error: ", error); })
    };

    getDataById = (id) => {
        const selectedItems = this.state.userList.find((values) => values.id === id);
        if (selectedItems) {
            this.setState({
                ...selectedItems,
                principal: {
                    id: selectedItems.principal.id
                },
                distributor: selectedItems.distributor,
                distributorOption: this.getDistributorByPrincipal2(selectedItems.principal.id),
                isDisabled: true,
                isEditable: true,
                error: {},
                userEdit: selectedItems,
            });
        }
    };

    //ketika create dimana principal baru dipilih
    getDistributorByPrincipal = (event) => {
        let selectedItems = this.state.distributorList
        .filter((data)=> data.principal.id === Number(event.target.value));
        if (selectedItems) {
            this.setState((prevState) => ({
                ...prevState,
                principal : {
                    id: event.target.value
                },
                distributorOption : selectedItems
            }));
        }
    }

    // ketika edit dimana data berdasarkan principal yang dipilih
    getDistributorByPrincipal2 = (e) => {
        let selectedItems = []
        for(const data of this.state.distributorList) {
            if(String(data.principal.id) === String(e)){
                selectedItems.push(data);
            }
        }
        return selectedItems
    }

    deleteData = () => {
        let confirmation = window.confirm(
            "Are you sure you want to delete " + this.state.fullName + "?"
        );
        if (confirmation === true) {
            API.delete("/users", this.state.id)
                .then((res) => {
                    if (res.status === 200) {
                        alert("User has been successfully deleted!");
                        window.location.reload();
                    }
                })
                .catch(error => { console.log("Error: ", error); })
        }
    };

    cancelEdit = () => {
        let data = this.state.userEdit;
        this.setState({
            ...data,
            error: {},
            isDisabled: true,
        });
    };

    isValidate = () => {
        let validate = {
            userIdError : "",
            fullNameError: "",
            passwordError : "",
            addressError : "",
            phoneError : "",
            emailError : "",
            statusError : "",
            principalError : "",
            regDateError: "",            
        };
        let id = this.state.userList.map((data) => data.userList);

        if (!this.state.userId) {
            validate.userIdError = "*User ID is required";
        }
        if(id.includes(this.state.userId) && !this.state.id){
            validate.userIdError = "This User ID has already been taken";
        }
        if (!validUserID.test(this.state.userId)) {
            validate.userIdError = "*User ID Only Can Be Letter, Number, Space, & Dot"
        }
        if (!this.state.fullName) {
            validate.fullNameError = "*Full Name is required";
        }
        if (!validLetterSpace.test(this.state.fullName)) {
            validate.fullNameError = "*Full Name Only Can Be Letters And Spaces";
        }
        if (!this.state.password) {
            validate.passwordError = "*Password is required";
        }
        if (!validPassword.test(this.state.password)) {
            validate.passwordError = "*Password Min 8 Max 20, At Least 1 Capital Letter & 1 Number";
        }
        if (!this.state.address) {
            validate.addressError = "*Address is required";
        }
        if (this.state.address.length > 255) {
            validate.addressError = "*Address Length Can't Be More Than 255";
        }
        if (!this.state.phone) {
            validate.phoneError = "*Phone is required";
        }
        if (!validPhone.test(this.state.phone)) {
            validate.phoneError = "*Invalid Phone Format, ex: +62/62/08xxxxxxxx";
        }
        if (!validEmail.test(this.state.email)) {
            validate.emailError = "*Invalid Email Format, ex: username@domain.com";
        }
        if (!validLetterSpace.test(this.state.status)) {
            validate.statusError = "*Full Name Only Can Be Letters And Spaces";
        }
        if (!this.state.principal.id) {
            validate.principalError = "*Principal is required";
        }
        if (!this.state.registrationDate) {
            validate.regDateError = "*Registration Date is required";
        }

        if (validate.userIdError || validate.fullNameError || validate.passwordError || validate.addressError ||
            validate.phoneError || validate.emailError || validate.statusError || validate.principalError || 
            validate.regDateError) {
            this.setState({
                error: validate,
            });
            return false;
        }
        return true;
    };

    render(){
        if (this.props.token !== "") {
        return(
            <>
                <Topnav section="USER" user={this.props.currentUser}/>
                <Navbar/>
                <ActionMenu
                    {...this.state} 
                    delete={this.deleteData} 
                    submitHandler={this.handleSubmit}
                    editHandler={this.handleEdit} 
                    cancelEdit={this.cancelEdit} 
                    refreshForm={this.refreshStateToEmpty}
                />

                <UserSideContent
                    getId={this.getDataById}
                    dataUser={this.state.userList}
                />
                <UserFormContent
                    {...this.state}
                    getPrincipal={this.getDistributorByPrincipal}
                    minDate = {DateTime("date")}
                    handleChange={this.handleChange}
                    handleChange2={this.handleChange2}
                />
            </>
        )
        }else{
            return(
                <Redirect to="/login"/>
            )
        }
    }
};

const mapStateToProps = (state) => {
    return {
        token: state.token,
        currentUser: state.username,
    }
};

export default connect(mapStateToProps)(Database);
