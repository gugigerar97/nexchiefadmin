import { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from 'react-redux';
import { getToken } from "../redux/actions/actions";

class Home extends Component {
    logout = () => {
        this.props.setToken("");
    }

    render() {
        if (this.props.token !== "") {
            return(
                <>
                    <div className="container_home">
                        <div className="wrapper-home">
                            <img src="images/logo-nexsoft.png" alt="logo-nexsoft"/>
                            <p>Main Menu</p>
                            <ul>
                                <li>
                                    <Link to="/principal">
                                        <div className="box">
                                            <i className="fas fa-box-open"></i>
                                            <p>Principal</p>
                                        </div>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/distributor">
                                        <div className="box">
                                            <i className="fas fa-truck"></i>
                                            <p>Distributor</p>
                                        </div>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/user">
                                        <div className="box">
                                            <i className="fas fa-user-circle"></i>
                                            <p>User</p>
                                        </div>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/database">
                                        <div className="box">
                                            <i className="fas fa-database"></i>
                                            <p>Database</p>
                                        </div>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="login" onClick={this.logout}>
                                        <div className="box">
                                            <i className="fas fa-power-off"></i>
                                            <p>Logout</p>
                                        </div>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </>
            )
        }else{
            return(
                <Redirect to="/login"/>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setToken: (value) => dispatch(getToken(value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);