import { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import Topnav from "../components/Topnav/Topnav";
import Navbar from "../components/Navbar/Navbar";
import ActionMenu from "../components/ActionMenu/ActionMenu";
import DistributorSideContent from "../components/SideContent/DistributorSideContent";
import DistributorFormContent from "../components/FormContent/DistributorFormContent";
import API from "../services/APIServices";
import DateTime from "../utils/DateTime";
import { validDistributorID, validEmail, validFax, validLetterSpace, validName, validPhone, validURL } from "../utils/Regex";

class Distributor extends Component {
    constructor(props){
        super(props);
        this.state={
            id: "",
            distributor_id: "",
            principal: {
                id: ""
            },
            name: "",
            address: "",
            city: "",
            ownerFName: "",
            ownerLName: "",
            email: "",
            phone: "",
            fax: "",
            country: "",
            contactFName: "",
            contactLName: "",
            contactPhone: "",
            contactEmail: "",
            website: "",
            createdAt: DateTime("dateTime"),
            createdBy: this.props.currentUser,
            updateBy: this.props.currentUser,
            updateAt: DateTime("dateTime"),
            error: {},
            distributorEdit: {},
            isDisabled: false,
            isEditable: false,
            distributorList: [],
            principalList: [],
        }
        this.baseState = {...this.state};
    }

    componentDidMount() {
        API.getAll("/distributor")
            .then(res => {
                if(res.status === 200){
                    this.setState({distributorList: res.data})
                }
            })
            .catch(error => { console.log("Error: ", error); })
        API.getAll("/principal")
            .then(res => {
                if(res.status === 200){
                    this.setState({principalList: res.data})
                }
            })
            .catch(error => { console.log("Error: ", error); })
    }

    refreshStateToEmpty = () => {
        this.setState({
            ...this.baseState,
            distributorList: this.state.distributorList,
            principalList: this.state.principalList
        });
    };

    data(type) {
        let json = {
            ...this.state,
            principal: {
                id: this.state.principal.id
            },
            createdAt: type === "create" ? DateTime("dateTime") : this.state.createdAt,
            updateBy: this.props.currentUser,
            updateAt: DateTime("dateTime"),
        };
        delete json.error;
        delete json.distributorEdit;
        delete json.isDisabled;
        delete json.isEditable;
        delete json.distributorList;
        delete json.principalList;
        return json;
    }

    handleChange = (event) => {
        const {name, value} = event.target;
        if(event.target.id === "principal"){
            this.setState((prevState) => ({
                ...prevState,
                principal : {
                    id: value
                },
            }));
        }else{
            this.setState((prevState) => ({
                ...prevState,
                [name]: value,
            }));
        }
    };

    handleSubmit = (event) => {
        event.preventDefault();
        const isValid = this.isValidate();        
        if (isValid) {
            if (this.state.id === "") {                
                this.createDistributor();
            } else {
                this.updateDistributor();
            }
        }
    };

    handleEdit = (event) => {
        event.preventDefault();
        this.setState({ isDisabled: false, isEditable: true });
    };

    createDistributor = () => {
        API.create("/distributor", this.data("create"))
        .then(res => {
            if(res.data.status === 200){
                alert(res.data.messages);
                window.location.reload();
            }
        })
        .catch(error => { console.log("Error: ", error); })
    }

    updateDistributor = () => {
        API.update("/distributor", this.state.id, this.data("update"))
        .then(res => {
            if(res.data.status === 200){
                alert(res.data.messages);
                window.location.reload();
            }
        })
        .catch(error => { console.log("Error: ", error); })
    }

    getDataById = (id) => {
        const selectedItems = this.state.distributorList.find((values) => values.id === id);
        if (selectedItems) {
            this.setState({
                ...selectedItems,
                principal: {
                    id: selectedItems.principal.id
                },
                isDisabled: true,
                isEditable: true,
                error: {},
                distributorEdit: selectedItems,
            });
        }
    };

    deleteData = () => {
        let confirmation = window.confirm(
            "Are you sure you want to delete " + this.state.name + "?"
        );
        if (confirmation === true) {
            API.delete("/distributor", this.state.id)
                .then((res) => {
                    if (res.status === 200) {
                        alert("Distributor has been successfully deleted!");
                        window.location.reload();
                    }
                })
                .catch(error => { console.log("Error: ", error); })
        }
    };

    cancelEdit = () => {
        let data = this.state.distributorEdit;
        this.setState({
            ...data,
            error: {},
            isDisabled: true,
        });
    };

    isValidate = () => {
        let validate = {
            principalError : "",
            distributorIdError: "",
            nameError : "",
            addressError : "",
            cityError : "",
            ownerFNameError: "",
            ownerLNameError: "",
            emailError : "",
            phoneError : "",
            faxError : "",
            countryError : "",
            contactFNameError: "",
            contactLNameError: "",
            contactPhoneError: "",
            contactEmailError : "",
            websiteError : "",
        };
        let id = this.state.distributorList.map((data) => data.distributor_id);
        if (!this.state.principal.id) {
            validate.principalError = "*Principal is required";
        }
        if(id.includes(this.state.distributor_id) && !this.state.id){
            validate.distributorIdError = "This Distributor ID has already been taken";
        }
        if (!validDistributorID.test(this.state.distributor_id)) {
            validate.distributorIdError = "*Distributor ID Only Can Be Number, Min & Max Length 7";
        }
        if (!this.state.name) {
            validate.nameError = "*Distributor Name is required";
        }
        if (!validName.test(this.state.name)) {
            validate.nameError = "*Distributor Name Only Can Be Letter, Space, & Dot";
        }
        if (!this.state.address) {
            validate.addressError = "*Address is required";
        }
        if (this.state.address.length > 255) {
            validate.addressError = "*Address Length Can't Be More Than 255";
        }
        if (!this.state.city) {
            validate.cityError = "*City is required";
        }
        if (!validLetterSpace.test(this.state.city)) {
            validate.cityError = "*City Only Can Be Letters And Spaces";
        }
        if (!this.state.ownerFName) {
            validate.ownerFNameError = "*Owner First Name is required";
        }
        if (!validLetterSpace.test(this.state.ownerFName)) {
            validate.ownerFNameError = "*Owner First Name Only Can Be Letters And Spaces"
        }
        if (!this.state.ownerLName) {
            validate.ownerLNameError = "*Owner Last Name is required";
        }
        if (!validLetterSpace.test(this.state.ownerLName)) {
            validate.ownerLNameError = "*Owner Last Name Only Can Be Letters And Spaces"
        }
        if (!validEmail.test(this.state.email)) {
            validate.emailError = "*Invalid Email Format, ex: username@domain.com";
        }
        if (!this.state.phone) {
            validate.phoneError = "*Phone is required";
        }
        if (!validPhone.test(this.state.phone)) {
            validate.phoneError = "*Invalid Phone Format, ex: +62/62/08xxxxxxxx";
        }
        if (!validFax.test(this.state.fax)) {
            validate.faxError = "*Invalid Fax Format, ex: (0xx)xxxxxxx"
        }
        if (!validLetterSpace.test(this.state.country)) {
            validate.countryError = "*Country Only Can Be Letters And Spaces"
        }
        if (!validLetterSpace.test(this.state.contactFName)) {
            validate.contactFNameError = "*Contact First Name Only Can Be Letters And Spaces"
        }
        if (!validLetterSpace.test(this.state.contactLName)) {
            validate.contactLNameError = "*Contact Last Name Only Can Be Letters And Spaces"
        }
        if (!validPhone.test(this.state.contactPhone)) {
            validate.contactPhoneError = "*Invalid Contact Phone Format, ex: +62/62/08xxxxxxxx";
        }
        if (!validEmail.test(this.state.contactEmail)) {
            validate.contactEmailError = "*Invalid Contact Email Format, ex: username@domain.com";
        }
        if (!validURL.test(this.state.website)) {
            validate.websiteError = "*Invalid Contact Email Format, ex: username@domain.com";
        }
        if (validate.principalError || validate.distributorIdError || validate.nameError || validate.cityError || 
            validate.addressError || validate.phoneError || validate.ownerFNameError || validate.ownerLNameError ||
            validate.contactFNameError || validate.contactLNameError || validate.contactPhoneError || validate.contactEmailError
            || validate.websiteError || validate.contactEmailError || validate.faxError) {
            this.setState({
                error: validate,
            });
            return false;
        }
        return true;
    };

    render(){
        if (this.props.token !== "") {
            return(
                <>
                    <Topnav section="DISTRIBUTOR" user={this.props.currentUser}/>
                    <Navbar/>
                    <ActionMenu 
                        {...this.state} 
                        delete={this.deleteData} 
                        submitHandler={this.handleSubmit}
                        editHandler={this.handleEdit} 
                        cancelEdit={this.cancelEdit} 
                        refreshForm={this.refreshStateToEmpty}
                    />

                    <DistributorSideContent
                        getId={this.getDataById} 
                        dataDistributor={this.state.distributorList}
                    />
                    <DistributorFormContent 
                        {...this.state}
                        handleChange={this.handleChange}
                    />
                </>
            )
        }else{
            return(
                <Redirect to="/login"/>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.token,
        currentUser: state.username,
    }
}

export default connect(mapStateToProps)(Distributor);
