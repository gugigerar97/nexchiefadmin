import { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { getCurrentUser, getToken } from "../redux/actions/actions";


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",            
            messageError: ""
        };
    }

    handleFormChange = (event) => {
        this.setState({[event.target.id]: event.target.value})
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.postDataAPI();
    }

    postDataAPI = () => {
        const requestOptions = {
            method: 'POST',
            headers: { "Content-type": "application/json; charset=UTF-8" },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
            })
        };
        
        fetch('http://localhost:8081/api/admin/login', requestOptions)
            .then(response => response.json())
            .then(response => {
                if (response.payload !== null) {
                    console.log(response);
                    this.props.setToken(response.accessToken);
                    this.props.currentUser(response.username);
                    alert("Login Success");
                    window.location.reload();
                }else{
                    this.setState({messageError: response.messages[0]})
                }
            })
            .catch(error => {
                this.setState({messageError: "username & password invalid"})
                console.log('There was an error :', error);
            });
    }

    errorMessage(){
        if (this.state.messageError !== "") {
            return (
            <p style={{color: "red", textAlign: "center", marginTop: '5px'}}>
                {this.state.messageError}
            </p>)
        }
    }

    render() {
        if (this.props.token === "") {    
            return(
                <div className="login">
                    <img src="images/logo-nexsoft.png" alt="logo-nexsoft"/>
                    <p>Enter Your Username & Password</p>
                    {this.errorMessage()}
                    <form method="post">
                        <input type="text" placeholder="Username" id="username" value={this.state.username} onChange={this.handleFormChange} required />
                        <input type="password" placeholder="Password" id="password" value={this.state.password} onChange={this.handleFormChange} required />
                        <button type="submit" className="btn-primary btn-block btn-large" onClick={this.handleSubmit}>Login</button>
                    </form>
                </div>
            )        
        }else {
            return <Redirect to="/"/>
        }
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.token
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setToken: (value) => dispatch(getToken(value)),
        currentUser: (value) => dispatch(getCurrentUser(value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);