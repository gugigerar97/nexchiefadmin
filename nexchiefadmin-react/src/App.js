import { Switch, Route, BrowserRouter } from "react-router-dom";
import Login from './pages/Login';
import Home from './pages/Home';
import Principal from './pages/Principal';
import Distributor from './pages/Distributor';
import User from "./pages/User";
import Database from "./pages/Database/Database";

import './assets/css/Style.css';

function App() {
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/principal" component={Principal} />
          <Route path="/distributor" component={Distributor} />
          <Route path="/user" component={User} />
          <Route path="/database" component={Database} />
          <Route path="/login" component={Login} />
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
