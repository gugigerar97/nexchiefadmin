import ActionType from "../constants/action-types";

export const getToken = (value) => {
    return {
        type: ActionType.GET_TOKEN_LOGIN,
        payload: value
    }
}

export const getCurrentUser = (value) => {
    return {
        type: ActionType.GET_CURENT_USER,
        payload: value
    }
}

export const handleSearch = (value) => {
    return {
        type: ActionType.HANDLE_SEARCH,
        payload: value
    }
}