import { createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist"
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web

import rootReducer from "../reducers/reducers";

const persistConfig = { // configuration object for redux-persist
    key: 'root',
    storage,
    blacklist: [] // will not be persisted
}

const persistedReducer = persistReducer(persistConfig, rootReducer) // create a persisted reducer

// STORE
const store = createStore(
    persistedReducer // pass the persisted reducer instead of rootReducer to createStore
);

const persistor = persistStore(store); // used to create the persisted store

export {store, persistor};