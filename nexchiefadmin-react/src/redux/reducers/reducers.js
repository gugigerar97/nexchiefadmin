import ActionType from "../constants/action-types";

const initialState = {
    token: "",
    username: "",
    search: "",
};

// REDUCER
const rootReducer = (state = initialState, action) => {
    switch(action.type){        
        case ActionType.GET_TOKEN_LOGIN: return {
            ...state,
            token: action.payload
        }
        case ActionType.GET_CURENT_USER: return {
            ...state,
            username: action.payload
        }
        case ActionType.HANDLE_SEARCH: return {
            ...state,
            search: action.payload
        }
        default: return state;
    }
};

export default rootReducer;