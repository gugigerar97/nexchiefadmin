const ActionType = {
    GET_CURENT_USER : "GET_CURENT_USER",
    GET_TOKEN_LOGIN : "GET_TOKEN_LOGIN",
    HANDLE_SEARCH : "HANDLE_SEARCH",
}

export default ActionType;