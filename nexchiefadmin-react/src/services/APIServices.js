import axios from "axios";

const BASE_URL = "http://localhost:8081/api";

class ApiServices {
    getAll(url) {
        return axios.get(BASE_URL + url);
    }

    get(url, id) {
        return axios.get(BASE_URL + `${url}/${id}`);
    }

    create(url, data) {
        return axios.post(BASE_URL + url, data);
    }

    update(url, id, data) {
        return axios.put(BASE_URL + `${url}/${id}`, data);
    }

    delete(url, id) {
        return axios.delete(BASE_URL + `${url}/${id}`);
    }

}

export default new  ApiServices();