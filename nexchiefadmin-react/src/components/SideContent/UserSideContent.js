import { Component } from "react";
import { connect } from "react-redux";
import './SideContent.css';

class SideContent extends Component {
    render(){
        return(
            <>
                <div className="side_content">
                    <ul>
                        {
                            this.props.dataUser.filter(data => {
                                if(this.props.search === "") {
                                    return data;
                                } else if (
                                    data.fullName.toLowerCase().includes(this.props.search.toLowerCase()) ||
                                    data.userId.toLowerCase().includes(this.props.search.toLowerCase()) ||
                                    data.principal.principal_id.toLowerCase().includes(this.props.search.toLowerCase()) ||
                                    data.status.toLowerCase().includes(this.props.search.toLowerCase()) ||
                                    data.productValidThru.toLowerCase().includes(this.props.search.toLowerCase())
                                    ) {
                                    return data;
                                }
                                return null;
                            }).map((data, key) => (
                                <li tabIndex='1' className='listData' key={key} onClick={() => this.props.getId(data.id)}>
                                    <img src="images/icons/icon_user.png" alt="logo-user"/>
                                    <div className="parrentDetail">
                                        <div className="wrapDetail" style={{lineHeight: "17px", marginTop: "-1px"}}>
                                            <span>{data.fullName}</span>
                                            <br/>
                                            <span className="sub">
                                                {data.userId}
                                                <span style={{color:"darkturquoise"}}>.{data.principal.principal_id}</span>
                                            </span>
                                        </div>
                                        <div className="wrapDetail" style={{textAlign:"right", marginTop: "-11px"}}>
                                            <span className="sub">{data.id}</span><br/>
                                            <span className="sub" style={{color:"limegreen"}}>{data.productValidThru}</span><br/>
                                            <span className="sub">{data.status}</span>
                                        </div>
                                    </div>
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        search: state.search,
    }
}

export default connect(mapStateToProps)(SideContent);