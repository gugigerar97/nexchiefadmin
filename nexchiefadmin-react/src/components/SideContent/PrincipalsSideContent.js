import { Component } from "react";
import { connect } from "react-redux";
import './SideContent.css';

class SideContent extends Component {
    render(){
        return(
            <>
                <div className="side_content">
                    <ul>
                        {
                            this.props.dataPrincipal.filter(data => {
                                if(this.props.search === "") {
                                    return data;
                                } else if (data.principal_id.toLowerCase().includes(this.props.search.toLowerCase()) ||
                                    data.principal_name.toLowerCase().includes(this.props.search.toLowerCase())) {
                                    return data;
                                }
                                return null;
                            }).map((data, key) => (
                                <li tabIndex='1' className='listData' key={key} onClick={() => this.props.getId(data.id)}>
                                    <img src="images/icons/icon_principal.png" alt="logo-principal"/>
                                    <div style={{lineHeight: '17px', top: '-5px'}}>
                                        <span><b>{data.principal_name}</b></span>
                                        <br/>
                                        <span style={{fontSize: "15px"}}>{data.principal_id}</span>
                                    </div>
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        search: state.search,
    }
}

export default connect(mapStateToProps)(SideContent);