import { Component } from "react";
import { connect } from "react-redux";
import './SideContent.css';

class SideContent extends Component {
    render(){
        return(
            <>
                <div className="side_content">
                    <ul>
                        {
                            this.props.dataDistributor.filter(data => {
                                if(this.props.search === "") {
                                    return data;
                                } else if (data.distributor_id.includes(this.props.search.toLowerCase()) ||
                                    data.name.toLowerCase().includes(this.props.search.toLowerCase()) ||
                                    data.city.toLowerCase().includes(this.props.search.toLowerCase())) {
                                    return data;
                                }
                                return null;
                            }).map((data, key) => (
                                <li tabIndex='1' className='listData' key={key} onClick={() => this.props.getId(data.id)}>
                                    <img className="img-distributor" src="images/icons/icon_distributor.png" alt="logo-distributor"/>
                                    <div>
                                        <div className="wrapDetail">
                                            <span className="top">{data.name}</span><br/>
                                            <span className="sub">
                                                {data.city}
                                            </span><br/>
                                            <span className="sub">
                                                {data.distributor_id} - {data.distributor_id}
                                            </span>
                                        </div>
                                    </div>
                                </li>
                            ))
                        }
                    </ul>
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        search: state.search,
    }
}

export default connect(mapStateToProps)(SideContent);