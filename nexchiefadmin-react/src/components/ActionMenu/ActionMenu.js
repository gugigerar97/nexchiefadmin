import { Component } from "react";
import { connect } from "react-redux";
import { handleSearch } from "../../redux/actions/actions";
import './ActionMenu.css';

class ActionMenu extends Component {    
    handleChange = (event) => {
        this.props.handleSearch(event.target.value);
    };

    resetSearch = () => {
        this.props.handleSearch("");
    }

    render(){
        return(
            <>
                <div className="action_menu">
                    <ul>
                        {
                            this.props.isEditable === true && this.props.isDisabled === false ? 
                            (
                                <>
                                    <li>
                                        <input id="cancelButton" type="submit" value='Cancel' onClick={this.props.cancelEdit}/>
                                    </li>
                                    <li>
                                        <input id="saveButton" type="submit" value='Save' onClick={this.props.submitHandler}/>
                                    </li>
                                </>
                            ) : 
                            (
                                <>
                                    <li>
                                        <input id="addButton" type="submit" value={this.props.id === "" ? "Submit" : "Add"}
                                        onClick={this.props.id === "" ? this.props.submitHandler : this.props.refreshForm}/>
                                    </li>
                                    {this.props.id === "" ? null : (
                                        <>
                                            <li>
                                                <input id="editButton" type="submit" value="Edit" onClick={this.props.editHandler}/>
                                            </li>
                                            <li>
                                                <input id="deleteButton" type="submit" value="Delete" onClick={this.props.delete}/>
                                            </li>
                                        </>
                                    )}
                                </>
                            )   
                        }
                    </ul>

                    <div className="search-container">
                        <div className="search">
                            <input type="text" className="searchTerm" placeholder="Search.." name="search" value={this.props.search}
                            onChange={this.handleChange}/>
                            {this.props.search === "" ? null : 
                                (
                                    <button type="submit" className="searchButton" onClick={this.resetSearch}>
                                        <i className="fas fa-window-close"></i>
                                    </button>
                                )
                            }
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        search: state.search,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleSearch: (value) => dispatch(handleSearch(value))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActionMenu);