import { Component } from "react";
import './FormContent.css';

export default class FormContent extends Component {
    render(){
        return(
            <>
                <div className="form_content">
                    <form>
                    <div className="row" style={{paddingTop: "10px"}}>
                            <div className="col1 first_field">
                                <label>User ID <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">
                                <input type="text" name="userId" placeholder="Enter User ID" value={this.props.userId} 
                                    onChange={this.props.handleChange} disabled={this.props.isEditable === true ? true: false}
                                />
                                <p className="errorMsg">{this.props.error.userIdError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>User Full Name <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">
                                <input type="text" name="fullName" placeholder="Enter User Name" value={this.props.fullName}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.fullNameError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Password <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">                                
                                <input type="password" name="password" minLength="8" placeholder="Enter Password"
                                    value={this.props.password} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.passwordError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Address <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">
                                <textarea name="address" placeholder="Enter Address" value={this.props.address}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.addressError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label className="after_address">Phone <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="phone" className="after_address" placeholder="Enter Phone" 
                                    value={this.props.phone} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.phoneError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Email </label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="email" placeholder="Enter Email" value={this.props.email} 
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.emailError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Status </label>
                            </div>
                            <div className="col2">
                                <input type="text" name="status" placeholder="Enter Status" value={this.props.status} 
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.statusError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Principal <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">
                                <select id="principal" value={this.props.principal.id}
                                    onChange={this.props.getPrincipal} disabled={this.props.isDisabled}>
                                    <option value="" hidden>-- SELECT PRINCIPAL --</option>
                                    {
                                        this.props.principalList.map((data) => {
                                            return(
                                                <option value={data.id} key={data.id}>                                          
                                                    {data.principal_id} - {data.principal_name}
                                                </option>
                                            )
                                        })
                                    }
                                </select>
                                <p className="errorMsg">{this.props.error.principalError}</p>
                            </div>
                        </div>
                        <div className="row" >
                            <div className="col1">
                                <label>Distributor</label>
                            </div>
                            <div className="col2">
                                <select id="distributor" onChange={this.props.handleChange}
                                    value={this.props.distributor.length > 1 ? "" : this.props.distributor[0].id}
                                    disabled={this.props.principal.id === "" ? true : this.props.isDisabled}>
                                    <option value="">-- SELECT ALL --</option>
                                    {
                                        this.props.distributorOption.map((data) => {
                                            return(
                                                <option value={data.id} key={data.id}>                                          
                                                    {data.name}
                                                </option>
                                            )
                                        })
                                    }
                                </select>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Disable Login </label>
                            </div>
                            <div className="col2">                                    
                                <div className="button b2" id="toggleButton">
                                    <input type="checkbox" className="checkbox" id="disableLogin" 
                                        checked={this.props.disableLogin} onChange={this.props.handleChange}/>
                                    <div className="knobs"></div>
                                    <div className="layer"></div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Registration Date</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="registrationDate" value={this.props.registrationDate} disabled/>
                                <p className="errorMsg">{this.props.error.regDateError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Product Valid Thru</label>
                            </div>
                            <div className="col2">                                
                                <input type="date" name="productValidThru" min={this.props.minDate} disabled={this.props.isDisabled}
                                    value={this.props.productValidThru} onChange={this.props.handleChange} 
                                />                                
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Created At</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="createdAt" value={this.props.createdAt} disabled/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Created By</label>
                            </div>
                            <div className="col2">
                                <input type="text" name="createdBy" value={this.props.createdBy} disabled/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Updated By</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="updateBy" value={this.props.updateBy} disabled/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1 last_field">
                                <label style={{borderBottom: '0'}}>Updated At</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="updateAt" value={this.props.updateAt} disabled/>
                            </div>
                        </div>
                    </form>
                </div>
            </>
        )
    }
}