import { Component } from "react";
import './FormContent.css';

export default class FormContent extends Component {
    render(){
        return(
            <>
                <div className="form_content">
                    <form>
                        <div className="row" style={{paddingTop: "10px"}}>
                            <div className="col1 first_field">
                                <label>Principal <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">
                                <select id="principal" value={this.props.principal.id} 
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}>
                                    <option value="" hidden>-- SELECT PRINCIPAL --</option>
                                    {
                                        this.props.principalList.map((data) => {
                                            return(
                                                <option value={data.id} key={data.id}>                                          
                                                    {data.principal_id} - {data.principal_name}
                                                </option>
                                            )
                                        })
                                    }
                                </select>
                                <p className="errorMsg">{this.props.error.principalError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Distributor ID</label>
                            </div>
                            <div className="col2">
                                <input type="text" name="distributor_id" placeholder="Enter Distributor ID"
                                    value={this.props.distributor_id} onChange={this.props.handleChange} 
                                    disabled={this.props.isEditable === true ? true: false} 
                                />
                                <p className="errorMsg">{this.props.error.distributorIdError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Distributor Name <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">
                                <input type="text" name="name" placeholder="Enter Distributor Name" value={this.props.name}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.nameError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Address <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">
                                <textarea name="address" placeholder="Enter Address" value={this.props.address} 
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}/>
                                <p className="errorMsg">{this.props.error.addressError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label style={{marginTop: '7px'}}>City <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="city" placeholder="Enter City" value={this.props.city}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled} className="after_address"
                                />
                                <p className="errorMsg">{this.props.error.cityError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Owner First Name <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="ownerFName" placeholder="Enter Owner First Name" value={this.props.ownerFName}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.ownerFNameError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Owner Last Name <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="ownerLName" placeholder="Enter Owner Last Name" value={this.props.ownerLName}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.ownerLNameError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Email </label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="email"  placeholder="Enter Email" value={this.props.email}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.emailError}</p>
                            </div>                         
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Phone <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="phone" placeholder="Enter Phone" value={this.props.phone}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.phoneError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Fax</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="fax" placeholder="Enter Fax" value={this.props.fax}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.faxError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Country</label>
                            </div>
                            <div className="col2">
                                <input type="text" name="country" placeholder="Enter Country" value={this.props.country}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.countryError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Contact First Name</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="contactFName" placeholder="Enter Contact First Name"
                                    value={this.props.contactFName} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.contactFNameError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Contact Last Name</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="contactLName" placeholder="Enter Contact Last Name"
                                    value={this.props.contactLName} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.contactLNameError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Contact Phone</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="contactPhone" placeholder="Enter Contact Phone"
                                    value={this.props.contactPhone} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.contactPhoneError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Contact Email</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="contactEmail" placeholder="Enter Contact Email"
                                    value={this.props.contactEmail} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.contactEmailError}</p>
                            </div>                            
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Website</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="website" placeholder="Enter Website url"
                                    value={this.props.website} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.websiteError}</p>
                            </div>                            
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Created At</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="createdAt" value={this.props.createdAt} disabled/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Created By</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="createdBy" value={this.props.createdBy} disabled/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Updated By</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="updateBy" value={this.props.updateBy} disabled/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1 last_field">
                                <label style={{borderBottom: '0'}}>Updated At</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="updateAt" value={this.props.updateAt} disabled/>
                            </div>
                        </div>
                    </form>
                </div>
            </>
        )
    }
}