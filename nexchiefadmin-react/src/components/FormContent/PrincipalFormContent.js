import { Component } from "react";
import './FormContent.css';

export default class PrincipalFormContent extends Component {
    render(){
        return(
            <>
                <div className="form_content">
                    <form>
                        <div className="row" style={{paddingTop: "10px"}}>
                            <div className="col1 first_field">
                                <label>Principal ID</label>
                            </div>
                            <div className="col2">
                                <input type="text" name="principal_id" placeholder="Enter Principal ID" value={this.props.principal_id} 
                                    onChange={this.props.handleChange} disabled={this.props.isEditable === true ? true: false}
                                />
                                <p className="errorMsg">{this.props.error.principalIdError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Principal Name <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">
                                <input type="text" name="principal_name" placeholder="Enter Principal Name" value={this.props.principal_name}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.principalnameError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Address <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">
                                <textarea id="address" name="address" placeholder="Enter Address" value={this.props.address}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.addressError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label className="after_address">City <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">                                
                                <input type="text" id="city" name="city" placeholder="Enter City" value={this.props.city}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled} className="after_address"
                                />
                                <p className="errorMsg">{this.props.error.cityError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Email <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">                                
                                <input type="text" id="email" name="email"  placeholder="Enter Email" value={this.props.email}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.emailError}</p>
                            </div>                         
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Phone <span style={{color: 'red'}}>*</span></label>
                            </div>
                            <div className="col2">                                
                                <input type="text" id="phone" name="phone" placeholder="Enter Phone" value={this.props.phone}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.phoneError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Fax</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="fax" placeholder="Enter Fax" value={this.props.fax}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.faxError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Country</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="country" placeholder="Enter Country" value={this.props.country}
                                    onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.countryError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Contact First Name</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="contactFName" placeholder="Enter Contact First Name"
                                    value={this.props.contactFName} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.contactFNameError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Contact Last Name</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="contactLName" placeholder="Enter Contact Last Name"
                                    value={this.props.contactLName} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.contactLNameError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Contact Phone</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="contactPhone" placeholder="Enter Contact Phone"
                                    value={this.props.contactPhone} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.contactPhoneError}</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Contact Email</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="contactEmail" placeholder="Enter Contact Email"
                                    value={this.props.contactEmail} onChange={this.props.handleChange} disabled={this.props.isDisabled}
                                />
                                <p className="errorMsg">{this.props.error.contactEmailError}</p>
                            </div>                            
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Licensed Expiry Date</label>
                            </div>
                            <div className="col2">                                
                                <input type="date" name="expireDate" min={this.props.minDate} disabled={this.props.isDisabled}
                                    value={this.props.expireDate === null ? "": this.props.expireDate} onChange={this.props.handleChange} 
                                />
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Created At</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="createdAt" value={this.props.createdAt} disabled/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Created By</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="createdBy" value={this.props.createdBy} disabled/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1">
                                <label>Updated By</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="updateBy" value={this.props.updateBy} disabled/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col1 last_field">
                                <label style={{borderBottom: '0'}}>Updated At</label>
                            </div>
                            <div className="col2">                                
                                <input type="text" name="updateAt" value={this.props.updateAt} disabled/>
                            </div>
                        </div>
                    </form>
                </div>
            </>
        )
    }
}