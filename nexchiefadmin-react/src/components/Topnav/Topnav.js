import { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { getToken } from "../../redux/actions/actions";
import './Topnav.css';

class Topnav extends Component {
    logout = () => {
        this.props.setToken("");
    }

    render(){
        return(
            <>
                <div className="topnav">
                    <ul>
                        <span>NEXCHIEF ADMIN - {this.props.section}</span>
                        <li>
                            <Link to="login" onClick={this.logout}>
                                <i className="fas fa-power-off"></i>
                            </Link>
                        </li>
                        <li><p className="nameadmin">{this.props.user}</p></li>
                    </ul>
                </div>
            </>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setToken: (value) => dispatch(getToken(value))
    }
}

export default connect(null, mapDispatchToProps)(Topnav);