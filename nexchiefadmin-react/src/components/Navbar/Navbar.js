import { Component } from "react";
import './Navbar.css';

class Navbar extends Component {
    render(){
        return(
            <>
                <div className="navbar">
                    <a href="/">Home</a>
                    <a href="/principal">Principal</a>                
                    <a href="/distributor">Distributor</a>
                    <a href="/user">User</a>
                    <a href="/database">Database</a>
                </div>
            </>
        )
    }
}

export default Navbar;