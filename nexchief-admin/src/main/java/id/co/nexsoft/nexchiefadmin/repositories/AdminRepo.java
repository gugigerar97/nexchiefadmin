package id.co.nexsoft.nexchiefadmin.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.nexchiefadmin.models.Admin;

@Repository
public interface AdminRepo extends JpaRepository<Admin, Integer> {
    @Query("select a from Admin a where a.username = ?1")
    Admin findByUsername(String username);

    @Query(nativeQuery = true, value = "SELECT username FROM Admin")
    List<String> findAllUsername();

    Boolean existsByUsername(String username);
}
