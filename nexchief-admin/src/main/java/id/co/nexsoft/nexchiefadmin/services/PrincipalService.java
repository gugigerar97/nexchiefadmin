package id.co.nexsoft.nexchiefadmin.services;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import id.co.nexsoft.nexchiefadmin.dto.ResponseData;
import id.co.nexsoft.nexchiefadmin.exception.BadRequestException;
import id.co.nexsoft.nexchiefadmin.exception.HandleNotFoundException;
import id.co.nexsoft.nexchiefadmin.models.Principal;
import id.co.nexsoft.nexchiefadmin.repositories.AdminRepo;
import id.co.nexsoft.nexchiefadmin.repositories.PrincipalRepo;
import id.co.nexsoft.nexchiefadmin.utils.ErrorUtil;

@Service
@Transactional
public class PrincipalService {
    @Autowired
    PrincipalRepo repoPrincipal;

    @Autowired
    AdminRepo repoAdmin;

    // getAll
    public List<Principal> getAll() {
        List<Principal> principals = repoPrincipal.findAll();
        return principals;
    }

    // getByID
    public Principal getPrincipalById(int id) {
        Optional<Principal> principalData = repoPrincipal.findById(id);
        if (!principalData.isPresent()) {
            throw new HandleNotFoundException("Principal With ID : " + id + " Is Not Found");
        }
        return principalData.get();
    }

    // create
    public ResponseEntity<ResponseData<Principal>> insert(Principal principal, Errors errors) {
        ResponseData<Principal> response = new ResponseData<>();
        // validation
        if (errors.hasErrors()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.setMessages(ErrorUtil.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } else if (repoPrincipal.findAllPrincipalId().contains(principal.getPrincipal_id())) {
            throw new BadRequestException("This Principal ID has already been taken!");
        } else if (!repoAdmin.existsByUsername(principal.getCreatedBy())
                || !repoAdmin.existsByUsername(principal.getUpdateBy())) {
            throw new HandleNotFoundException("Created/Updated By Admin not found!");
        }
        // generate id when empty
        String principalName = principal.getPrincipal_name();
        if (principal.getPrincipal_id() == null || principal.getPrincipal_id().isBlank()) {
            principal.setPrincipal_id(generateId(principalName) + (repoPrincipal.findAllPrincipalId().size() + 1));
        }
        principal.setPrincipal_id(principal.getPrincipal_id().toUpperCase());
        principal.setPrincipal_name(principalName.toUpperCase());
        repoPrincipal.save(principal);

        response.setStatus(HttpStatus.OK.value());
        response.getMessages().add("Successfully Added Principal!");
        response.setPayload(principal);
        return ResponseEntity.ok(response);
    }

    // edit
    public ResponseEntity<ResponseData<Principal>> update(int id, Principal principal, Errors errors) {
        ResponseData<Principal> response = new ResponseData<>();
        Optional<Principal> principalData = repoPrincipal.findById(id);

        // validation
        if (!principalData.isPresent()) {
            throw new HandleNotFoundException("Principal With ID : " + id + " Is Not Found");
        } else if (errors.hasErrors()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.setMessages(ErrorUtil.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } else if (!repoAdmin.existsByUsername(principal.getCreatedBy())
                || !repoAdmin.existsByUsername(principal.getUpdateBy())) {
            throw new HandleNotFoundException("Created/Updated By Admin not found!");
        }
        // prevent id from being changed
        principal.setPrincipal_id(principalData.get().getPrincipal_id().toUpperCase());
        principal.setPrincipal_name(principal.getPrincipal_name().toUpperCase());
        repoPrincipal.save(principal);

        response.setStatus(HttpStatus.OK.value());
        response.getMessages().add("Successfully Update Principal!");
        response.setPayload(principal);
        return ResponseEntity.ok(response);
    }

    // generate ID
    public String generateId(String str) {
        StringBuilder acronym = new StringBuilder();
        String[] splitString = str.split(" ");
        if (splitString.length < 2) {
            acronym.append(str);
        } else {
            for (String word : splitString) {
                acronym.append(word.substring(0, 1));
            }
        }
        String resultId = acronym.toString().toUpperCase();
        return resultId;
    }

    // delete
    public void delete(int id) {
        Optional<Principal> principalData = repoPrincipal.findById(id);
        if (!principalData.isPresent()) {
            throw new HandleNotFoundException("Principal With ID : " + id + " Is Not Found");
        }
        repoPrincipal.deleteById(id);
    }
}