package id.co.nexsoft.nexchiefadmin.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.nexchiefadmin.models.Distributor;

@Repository
public interface DistributorRepo extends JpaRepository<Distributor, Integer> {
    @Query(nativeQuery = true, value = "SELECT distributor_id FROM distributor")
    List<String> findAllDistributorId();
}
