package id.co.nexsoft.nexchiefadmin.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.nexchiefadmin.dto.ResponseData;
import id.co.nexsoft.nexchiefadmin.models.User;
import id.co.nexsoft.nexchiefadmin.services.UserService;
import id.co.nexsoft.nexchiefadmin.utils.Config;

@RestController
@CrossOrigin(origins = Config.url)
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    UserService userService;

    // getAll
    @GetMapping
    public List<User> getAllUsers() {
        return userService.getAll();
    }

    // getByID
    @GetMapping("/{id}")
    public User getUser(@PathVariable("id") int id) {
        return userService.getUsersById(id);
    }

    // create
    @PostMapping
    public ResponseEntity<ResponseData<User>> insertUser(@Valid @RequestBody User user, Errors errors) {
        return userService.insert(user, errors);
    }

    // update
    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<User>> updateUser(@PathVariable("id") int id, @Valid @RequestBody User user,
            Errors errors) {
        return userService.update(id, user, errors);
    }

    // delete
    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") int id) {
        userService.delete(id);
    }
}
