package id.co.nexsoft.nexchiefadmin.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import id.co.nexsoft.nexchiefadmin.utils.Config;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@SQLDelete(sql = "UPDATE admin SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Admin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Username Required")
    @Size(max = 20, message = "Max Length 20")
    @Pattern(regexp = Config.REGEX_USER_ID, message = "Invalid Input Username")
    @Column(unique = true)
    private String username;

    @NotBlank(message = "Username Required")
    @Pattern(regexp = Config.REGEX_PASSWORD, message = "Password Min 8, At Least 1 Capital Letter & 1 Number")
    private String password;

    private String accessToken;

    private boolean deleted = Boolean.FALSE;
}
