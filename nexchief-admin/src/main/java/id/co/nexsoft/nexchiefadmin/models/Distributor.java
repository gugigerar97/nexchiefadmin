package id.co.nexsoft.nexchiefadmin.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import id.co.nexsoft.nexchiefadmin.utils.Config;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@SQLDelete(sql = "UPDATE distributor SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Distributor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Pattern(regexp = Config.REGEX_DISTRIBUTOR_ID, message = "Input Distributor ID Only Can Be Number, Min & Max  Length 7")
    @Column(unique = true)
    private String distributor_id;

    @ManyToOne()
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "principal_id", nullable = false)
    private Principal principal;

    @NotBlank(message = "Distributor Name Can't Be Empty")
    @Pattern(regexp = Config.REGEX_DISTRIBUTOR_NAME, message = "Invalid Principal ID Character Input")
    @Column(nullable = false)
    private String name;

    @NotBlank(message = "Address Can't Be Empty")
    @Size(max = 255, message = "Address Should Not Be Greater Than 255")
    @Column(columnDefinition = "TEXT", nullable = false)
    private String address;

    @NotBlank(message = "City Can't Be Empty")
    @Pattern(regexp = Config.REGEX_LETTER_SPACE, message = "Input City Only Can Be Letters & Spaces")
    @Column(nullable = false)
    private String city;

    @NotBlank(message = "Owner First Name Can't Be Empty")
    @Pattern(regexp = Config.REGEX_LETTER_SPACE, message = "Input Owner First Name Only Can Be Letters & Spaces")
    @Column(nullable = false)
    private String ownerFName;

    @NotBlank(message = "Owner Last Name Can't Be Empty")
    @Pattern(regexp = Config.REGEX_LETTER_SPACE, message = "Input Owner Last Name Only Can Be Letters & Spaces")
    @Column(nullable = false)
    private String ownerLName;

    @Pattern(regexp = Config.REGEX_EMAIL, message = "Invalid Email Input Format")
    @Column(nullable = false)
    private String email;

    @NotBlank(message = "Phone Can't Be Empty")
    @Pattern(regexp = Config.REGEX_PHONE, message = "Invalid Phone Input Format")
    @Column(nullable = false)
    private String phone;

    @Pattern(regexp = Config.REGEX_FAX, message = "Invalid Fax Character Input")
    private String fax;

    @Pattern(regexp = Config.REGEX_LETTER_SPACE, message = "Input Country Only Can Be Letters & Spaces")
    private String country;

    @Pattern(regexp = Config.REGEX_LETTER_SPACE, message = "Input Contact First Name Only Can Be Letters & Spaces")
    private String contactFName;

    @Pattern(regexp = Config.REGEX_LETTER_SPACE, message = "Input Contact Last Name Only Can Be Letters & Spaces")
    private String contactLName;

    @Pattern(regexp = Config.REGEX_PHONE, message = "Invalid Contact Phone Input Format")
    private String contactPhone;

    @Pattern(regexp = Config.REGEX_EMAIL, message = "Invalid Contact Email Input Format")
    private String contactEmail;

    @Pattern(regexp = Config.REGEX_URL, message = "Invalid URL Input Character")
    private String website;

    @NotBlank(message = "Created At Can't Be Empty")
    @Column(nullable = false)
    private String createdAt;

    @NotBlank(message = "Created By Can't Be Empty")
    @Column(nullable = false)
    private String createdBy;

    @NotBlank(message = "Updated By Can't Be Empty")
    @Column(nullable = false)
    private String updateBy;

    @NotBlank(message = "Updated At Can't Be Empty")
    @Column(nullable = false)
    private String updateAt;

    private boolean deleted = Boolean.FALSE;
}
