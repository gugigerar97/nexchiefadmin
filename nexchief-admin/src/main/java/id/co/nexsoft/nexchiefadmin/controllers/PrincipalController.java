package id.co.nexsoft.nexchiefadmin.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.nexchiefadmin.dto.ResponseData;
import id.co.nexsoft.nexchiefadmin.models.Principal;
import id.co.nexsoft.nexchiefadmin.services.PrincipalService;
import id.co.nexsoft.nexchiefadmin.utils.Config;

@RestController
@CrossOrigin(origins = Config.url)
@RequestMapping("/api/principal")
public class PrincipalController {
    @Autowired
    PrincipalService principalService;

    // getall
    @GetMapping
    public List<Principal> getAllPrincipals() {
        return principalService.getAll();
    }

    // getbyid
    @GetMapping("/{id}")
    public Principal getPrincipal(@PathVariable("id") int id) {
        return principalService.getPrincipalById(id);
    }

    // create
    @PostMapping
    public ResponseEntity<ResponseData<Principal>> insertPrincipal(@Valid @RequestBody Principal principal,
            Errors errors) {
        return principalService.insert(principal, errors);
    }

    // update
    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<Principal>> updatePrincipal(@PathVariable("id") int id,
            @Valid @RequestBody Principal principal, Errors errors) {
        return principalService.update(id, principal, errors);
    }

    // delete
    @DeleteMapping("/{id}")
    public void deletePrincipal(@PathVariable("id") int id) {
        principalService.delete(id);
    }
}
