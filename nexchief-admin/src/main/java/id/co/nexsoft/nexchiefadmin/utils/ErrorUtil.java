package id.co.nexsoft.nexchiefadmin.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class ErrorUtil {

    public static List<String> parseErrors(Errors errors) {
        List<String> errorMessage = new ArrayList<>();
        for (ObjectError error : errors.getAllErrors()) {
            errorMessage.add(error.getDefaultMessage());
        }
        return errorMessage;
    }
}
