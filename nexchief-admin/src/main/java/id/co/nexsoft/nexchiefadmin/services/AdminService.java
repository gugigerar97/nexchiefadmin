package id.co.nexsoft.nexchiefadmin.services;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import id.co.nexsoft.nexchiefadmin.dto.ResponseData;
import id.co.nexsoft.nexchiefadmin.exception.BadRequestException;
import id.co.nexsoft.nexchiefadmin.exception.HandleNotFoundException;
import id.co.nexsoft.nexchiefadmin.models.Admin;
import id.co.nexsoft.nexchiefadmin.repositories.AdminRepo;
import id.co.nexsoft.nexchiefadmin.utils.ErrorUtil;

@Service
@Transactional
public class AdminService {
    @Autowired
    AdminRepo repoAdmin;

    // getAll
    public List<Admin> getAll() {
        List<Admin> admins = repoAdmin.findAll();
        return admins;
    }

    // getByID
    public Admin getAdminById(int id) {
        Optional<Admin> admindata = repoAdmin.findById(id);
        if (!admindata.isPresent()) {
            throw new HandleNotFoundException("Admin With ID : " + id + " Is Not Found");
        }
        return admindata.get();
    }

    // create
    public ResponseEntity<ResponseData<Admin>> insert(Admin admin, Errors errors) {
        ResponseData<Admin> response = new ResponseData<>();
        // validation
        if (errors.hasErrors()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.setMessages(ErrorUtil.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } else if (repoAdmin.findAllUsername().contains(admin.getUsername())) {
            throw new BadRequestException("This Admin ID has already been taken!");
        }
        repoAdmin.save(admin);
        response.setStatus(HttpStatus.OK.value());
        response.getMessages().add("Successfully Added Admin!");
        response.setPayload(admin);
        return ResponseEntity.ok(response);
    }

    // Update
    public ResponseEntity<ResponseData<Admin>> update(int id, Admin admin, Errors errors) {
        ResponseData<Admin> response = new ResponseData<>();
        Optional<Admin> adminData = repoAdmin.findById(id);
        // validation
        if (!adminData.isPresent()) {
            throw new HandleNotFoundException("Admin With ID : " + id + " Is Not Found");
        }
        if (errors.hasErrors()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.setMessages(ErrorUtil.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        admin.setAccessToken(adminData.get().getAccessToken());
        repoAdmin.save(admin);
        response.setStatus(HttpStatus.OK.value());
        response.getMessages().add("Successfully Update Admin!");
        response.setPayload(admin);
        return ResponseEntity.ok(response);
    }

    // delete
    public void delete(int id) {
        Optional<Admin> adminData = repoAdmin.findById(id);
        if (!adminData.isPresent()) {
            throw new HandleNotFoundException("Admin With ID : " + id + " Is Not Found");
        }
        repoAdmin.deleteById(id);
    }

    // Login
    public Admin login(Admin admin) {
        Admin adminLogin = repoAdmin.findByUsername(admin.getUsername());
        if (adminLogin != null) {
            if (admin.getPassword().equals(adminLogin.getPassword())) {
                String token = generateRandomToken();
                adminLogin.setAccessToken(token);
                repoAdmin.save(adminLogin);
                return adminLogin;
            } else {
                throw new BadRequestException("Wrong Password Entered");
            }
        } else {
            throw new HandleNotFoundException("Admin Is Not Found");
        }
    }

    // generate random token
    public String generateRandomToken() {
        String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        int length = 32;
        StringBuilder builder = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            builder.append(ALPHABET.charAt(random.nextInt(ALPHABET.length())));
        }
        return builder.toString();
    }
}
