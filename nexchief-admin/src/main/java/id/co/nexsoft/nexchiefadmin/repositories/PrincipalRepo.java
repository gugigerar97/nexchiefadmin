package id.co.nexsoft.nexchiefadmin.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.nexchiefadmin.models.Principal;

@Repository
public interface PrincipalRepo extends JpaRepository<Principal, Integer> {
    @Query(nativeQuery = true, value = "SELECT principal_id FROM principal")
    List<String> findAllPrincipalId();

    @Query(nativeQuery = true, value = "SELECT deleted FROM principal WHERE id=?1")
    Boolean findDeletedPrincipal(int id);
}
