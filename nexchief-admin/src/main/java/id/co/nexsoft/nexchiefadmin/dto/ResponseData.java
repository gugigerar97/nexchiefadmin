package id.co.nexsoft.nexchiefadmin.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseData<T> {
    private int status;
    private List<String> messages = new ArrayList<>();
    private T payload;
}
