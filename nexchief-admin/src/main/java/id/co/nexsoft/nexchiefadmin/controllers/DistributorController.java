package id.co.nexsoft.nexchiefadmin.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.nexchiefadmin.dto.ResponseData;
import id.co.nexsoft.nexchiefadmin.models.Distributor;
import id.co.nexsoft.nexchiefadmin.services.DistributorService;
import id.co.nexsoft.nexchiefadmin.utils.Config;

@RestController
@CrossOrigin(origins = Config.url)
@RequestMapping("/api/distributor")
public class DistributorController {
    @Autowired
    DistributorService DistributorService;

    // getAll
    @GetMapping
    public List<Distributor> getAllDistributors() {
        return DistributorService.getAll();
    }

    // getByID
    @GetMapping("/{id}")
    public Distributor getDistributor(@PathVariable("id") int id) {
        return DistributorService.getDistributorById(id);
    }

    // create
    @PostMapping
    public ResponseEntity<ResponseData<Distributor>> insertDistributor(@Valid @RequestBody Distributor distributor,
            Errors errors) {
        return DistributorService.insert(distributor, errors);
    }

    // update
    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<Distributor>> updateDistributor(@PathVariable("id") int id,
            @Valid @RequestBody Distributor distributor, Errors errors) {
        return DistributorService.update(id, distributor, errors);
    }

    // delete
    @DeleteMapping("/{id}")
    public void deleteDistributor(@PathVariable("id") int id) {
        DistributorService.delete(id);
    }
}
