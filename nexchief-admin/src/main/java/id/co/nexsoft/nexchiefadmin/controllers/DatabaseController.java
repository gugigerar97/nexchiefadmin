package id.co.nexsoft.nexchiefadmin.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.nexchiefadmin.services.DatabaseService;
import id.co.nexsoft.nexchiefadmin.utils.Config;

@RestController
@CrossOrigin(origins = Config.url)
@RequestMapping("/api/database")
public class DatabaseController {
    @Autowired
    DatabaseService DatabaseService;

    @GetMapping("/export")
    public String exportDatabaseTo() throws InterruptedException {
        return DatabaseService.export();
    }
}
