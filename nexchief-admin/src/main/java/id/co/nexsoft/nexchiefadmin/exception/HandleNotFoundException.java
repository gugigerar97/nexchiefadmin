package id.co.nexsoft.nexchiefadmin.exception;

public class HandleNotFoundException extends RuntimeException {
    public HandleNotFoundException(String exception) {
        super(exception);
    }
}
