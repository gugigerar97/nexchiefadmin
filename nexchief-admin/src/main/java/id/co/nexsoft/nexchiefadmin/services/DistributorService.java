package id.co.nexsoft.nexchiefadmin.services;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import id.co.nexsoft.nexchiefadmin.repositories.AdminRepo;
import id.co.nexsoft.nexchiefadmin.repositories.DistributorRepo;
import id.co.nexsoft.nexchiefadmin.repositories.PrincipalRepo;
import id.co.nexsoft.nexchiefadmin.utils.ErrorUtil;
import id.co.nexsoft.nexchiefadmin.dto.ResponseData;
import id.co.nexsoft.nexchiefadmin.exception.BadRequestException;
import id.co.nexsoft.nexchiefadmin.exception.HandleNotFoundException;
import id.co.nexsoft.nexchiefadmin.models.Distributor;

@Service
@Transactional
public class DistributorService {
    @Autowired
    DistributorRepo repoDistributor;

    @Autowired
    PrincipalRepo repoPrincipal;

    @Autowired
    AdminRepo repoAdmin;

    // getAll
    public List<Distributor> getAll() {
        List<Distributor> distributors = repoDistributor.findAll();
        for (Distributor distributor : distributors) {
            if (distributor.getPrincipal() == null) {
                delete(distributor.getId());
            }
        }
        return distributors;
    }

    // getByID
    public Distributor getDistributorById(int id) {
        Optional<Distributor> distributorData = repoDistributor.findById(id);
        if (!distributorData.isPresent()) {
            throw new HandleNotFoundException("Distributor With ID : " + id + " Is Not Found");
        } else if (repoPrincipal.findDeletedPrincipal(distributorData.get().getPrincipal().getId())) {
            throw new HandleNotFoundException("Distributor With ID : " + id + " Is Not Found");
        }
        return distributorData.get();
    }

    // create
    public ResponseEntity<ResponseData<Distributor>> insert(Distributor distributor, Errors errors) {
        ResponseData<Distributor> response = new ResponseData<>();
        // validation
        if (errors.hasErrors()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.setMessages(ErrorUtil.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } else if (repoDistributor.findAllDistributorId().contains(distributor.getDistributor_id())) {
            throw new BadRequestException("This Distributor ID has already been taken!");
        } else if (distributor.getPrincipal().getId() < 1) {
            throw new BadRequestException("Principal Can't Be Empty");
        } else if (repoPrincipal.findDeletedPrincipal(distributor.getPrincipal().getId()) == true) {
            throw new BadRequestException("Selected Principal Not Found");
        } else if (!repoAdmin.existsByUsername(distributor.getCreatedBy())
                || !repoAdmin.existsByUsername(distributor.getUpdateBy())) {
            throw new HandleNotFoundException("Created/Updated By Admin not found!");
        }
        // generate id when empty
        if (distributor.getDistributor_id() == null || distributor.getDistributor_id().isBlank()) {
            int initialValue = 1000100;
            int increment = initialValue + (repoDistributor.findAllDistributorId().size() + 1);
            distributor.setDistributor_id(String.format("%d", increment));
        }
        distributor.setName(distributor.getName().toUpperCase());
        repoDistributor.save(distributor);

        response.setStatus(HttpStatus.OK.value());
        response.getMessages().add("Successfully Added Distributor!");
        response.setPayload(distributor);
        return ResponseEntity.ok(response);
    }

    // edit
    public ResponseEntity<ResponseData<Distributor>> update(int id, Distributor distributor, Errors errors) {
        ResponseData<Distributor> response = new ResponseData<>();
        Optional<Distributor> distributorData = repoDistributor.findById(id);
        // validation
        if (!distributorData.isPresent()) {
            throw new HandleNotFoundException("Distributor With ID : " + id + " Is Not Found");
        } else if (errors.hasErrors()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.setMessages(ErrorUtil.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } else if (distributor.getPrincipal().getId() < 1) {
            throw new BadRequestException("Principal Can't Be Empty");
        } else if (repoPrincipal.findDeletedPrincipal(distributor.getPrincipal().getId()) == true) {
            throw new BadRequestException("Selected Principal Not Found");
        } else if (!repoAdmin.existsByUsername(distributor.getCreatedBy())
                || !repoAdmin.existsByUsername(distributor.getUpdateBy())) {
            throw new HandleNotFoundException("Created/Updated By Admin not found!");
        }
        // prevent id from being changed
        distributor.setDistributor_id(distributorData.get().getDistributor_id());
        distributor.setName(distributor.getName().toUpperCase());
        distributor.setCreatedBy(distributorData.get().getCreatedBy());
        repoDistributor.save(distributor);

        response.setStatus(HttpStatus.OK.value());
        response.getMessages().add("Successfully Update Distributor!");
        response.setPayload(distributor);
        return ResponseEntity.ok(response);
    }

    // delete
    public void delete(int id) {
        Optional<Distributor> distributorData = repoDistributor.findById(id);
        if (!distributorData.isPresent()) {
            throw new HandleNotFoundException("Distributor With ID : " + id + " Is Not Found");
        }
        repoDistributor.deleteById(id);
    }
}
