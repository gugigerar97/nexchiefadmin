package id.co.nexsoft.nexchiefadmin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import id.co.nexsoft.nexchiefadmin.dto.ResponseData;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ResponseData<?>> handleAllExceptions(Exception ex) {
        ResponseData<?> response = new ResponseData<>();
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.getMessages().add(ex.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
    }

    @ExceptionHandler(BadRequestException.class)
    public final ResponseEntity<ResponseData<?>> handleBadRequest(Exception ex) {
        ResponseData<?> response = new ResponseData<>();
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        response.getMessages().add(ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }

    @ExceptionHandler(HandleNotFoundException.class)
    public ResponseEntity<ResponseData<?>> customHandleNotFound(Exception ex) {
        ResponseData<?> response = new ResponseData<>();
        response.setStatus(HttpStatus.NOT_FOUND.value());
        response.getMessages().add(ex.getMessage());
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }
}
