package id.co.nexsoft.nexchiefadmin.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.time.LocalDate;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class DatabaseService {
    static LocalDate today = LocalDate.now();
    static final String databaseName = "nexchiefadmin";
    static final String fileName = "nexchiefadmin (" + today + ") .sql";
    static final String userName = "root";
    static final String password = "root";
    static final String savePath = "C:/Users/user/Downloads/";

    public String export() {
        File saveFile = new File(savePath);
        if (!saveFile.exists()) {
            saveFile.mkdirs();
        }

        PrintWriter pw = null;
        BufferedReader bufferedReader = null;
        try {
            pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(savePath + fileName), "utf8"));
            Process process = Runtime.getRuntime().exec(
                    "mysqldump -u" + userName + " -p" + password + " --databases --set-charset=UTF8 " + databaseName);
            InputStreamReader inputStreamReader = new InputStreamReader(process.getInputStream(), "utf8");
            bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                pw.println(line);
            }
            pw.flush();
            if (process.waitFor() == 0) {
                return "Export Success";
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (pw != null) {
                    pw.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "Export Failed";
    }
}
