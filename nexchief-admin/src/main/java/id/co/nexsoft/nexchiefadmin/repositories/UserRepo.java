package id.co.nexsoft.nexchiefadmin.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.co.nexsoft.nexchiefadmin.models.User;

@Repository
public interface UserRepo extends JpaRepository<User, Integer> {
    @Query(nativeQuery = true, value = "SELECT user_id FROM user")
    List<String> findAllUserId();
}
