package id.co.nexsoft.nexchiefadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NexchiefAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(NexchiefAdminApplication.class, args);
	}

}
