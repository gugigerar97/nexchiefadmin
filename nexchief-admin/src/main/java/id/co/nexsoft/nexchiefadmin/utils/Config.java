package id.co.nexsoft.nexchiefadmin.utils;

public class Config {
    public final static String url = "http://localhost:3000";
    public final static String REGEX_DISTRIBUTOR_NAME = "^[a-zA-Z0-9.() ]*$";
    public final static String REGEX_DISTRIBUTOR_ID = "^$|^\\d{7}";
    public final static String REGEX_USER_ID = "^[a-zA-Z0-9. ]*$";
    public final static String REGEX_PASSWORD = "^(?=.*\\d)(?=.*[A-Z]).{8,20}$";
    public final static String REGEX_LETTER_NUMBER = "^[a-zA-Z0-9]*$";
    public final static String REGEX_LETTER_SPACE = "^[a-zA-Z ]*$";
    public final static String REGEX_EMAIL = "^$|^[\\w-.]+@[a-zA-Z0-9.-]+\\.[\\w-]{2,4}$";
    public final static String REGEX_PHONE = "^$|^(\\+62|62|0)8[1-9][0-9]{8,10}$";
    public final static String REGEX_FAX = "^$|^\\(0[1-9]{2}\\)[0-9]{7}$";
    public final static String REGEX_URL = "^$|^(http(s)?:\\/\\/)?[\\w]+\\.[\\w]+\\.[\\w\\.]+$";
}
