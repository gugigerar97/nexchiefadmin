package id.co.nexsoft.nexchiefadmin.models;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import id.co.nexsoft.nexchiefadmin.utils.Config;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@SQLDelete(sql = "UPDATE user SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "User ID Can't Be Empty")
    @Pattern(regexp = Config.REGEX_USER_ID, message = "User ID Invalid Input Character")
    @Column(unique = true, nullable = false)
    private String userId;

    @NotBlank(message = "Full Name Can't Be Empty")
    @Pattern(regexp = Config.REGEX_LETTER_SPACE, message = "Input Full Name Only Can Be Letters & Spaces")
    @Column(nullable = false)
    private String fullName;

    @NotBlank(message = "Password Can't Be Empty")
    @Pattern(regexp = Config.REGEX_PASSWORD, message = "Password Min 8, At Least 1 Capital Letter & 1 Number")
    @Column(nullable = false)
    private String password;

    @NotBlank(message = "Address Can't Be Empty")
    @Size(max = 255, message = "Address Should Not Be Greater Than 255")
    @Column(columnDefinition = "TEXT", nullable = false)
    private String address;

    @NotBlank(message = "Phone Can't Be Empty")
    @Pattern(regexp = Config.REGEX_PHONE, message = "Invalid Phone Input Format")
    @Column(nullable = false)
    private String phone;

    @Pattern(regexp = Config.REGEX_EMAIL, message = "Invalid Email Input Format")
    @Column(nullable = false)
    private String email;

    @Pattern(regexp = Config.REGEX_LETTER_SPACE, message = "Input City Only Can Be Letters & Spaces")
    private String status;

    @ManyToOne()
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "principal_id")
    private Principal principal;

    @ManyToMany()
    @NotFound(action = NotFoundAction.IGNORE)
    private List<Distributor> distributor;

    @Column(nullable = false)
    private boolean disableLogin;

    @NotBlank(message = "Registration Date Can't Be Empty")
    @Column(nullable = false)
    private String registrationDate;

    private LocalDate productValidThru;

    @NotBlank(message = "Created At Can't Be Empty")
    @Column(nullable = false)
    private String createdAt;

    @NotBlank(message = "Created By Can't Be Empty")
    @Column(nullable = false)
    private String createdBy;

    @NotBlank(message = "Updated By Can't Be Empty")
    @Column(nullable = false)
    private String updateBy;

    @NotBlank(message = "Updated At Can't Be Empty")
    @Column(nullable = false)
    private String updateAt;

    private boolean deleted = Boolean.FALSE;
}
