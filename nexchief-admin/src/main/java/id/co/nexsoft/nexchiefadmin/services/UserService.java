package id.co.nexsoft.nexchiefadmin.services;

import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import id.co.nexsoft.nexchiefadmin.dto.ResponseData;
import id.co.nexsoft.nexchiefadmin.exception.BadRequestException;
import id.co.nexsoft.nexchiefadmin.exception.HandleNotFoundException;
import id.co.nexsoft.nexchiefadmin.models.User;
import id.co.nexsoft.nexchiefadmin.repositories.AdminRepo;
import id.co.nexsoft.nexchiefadmin.repositories.DistributorRepo;
import id.co.nexsoft.nexchiefadmin.repositories.PrincipalRepo;
import id.co.nexsoft.nexchiefadmin.repositories.UserRepo;
import id.co.nexsoft.nexchiefadmin.utils.ErrorUtil;

@Service
@Transactional
public class UserService {
    @Autowired
    UserRepo repoUser;

    @Autowired
    PrincipalRepo repoPrincipal;

    @Autowired
    DistributorRepo repoDistributor;

    @Autowired
    AdminRepo repoAdmin;

    // getAll
    public List<User> getAll() {
        List<User> users = repoUser.findAll();
        for (User user : users) {
            if (user.getPrincipal() == null) {
                delete(user.getId());
            }
        }
        return users;
    }

    // getByID
    public User getUsersById(int id) {
        Optional<User> userdata = repoUser.findById(id);
        if (!userdata.isPresent()) {
            throw new HandleNotFoundException("Distributor With ID : " + id + " Is Not Found");
        } else if (repoPrincipal.findDeletedPrincipal(userdata.get().getPrincipal().getId())) {
            throw new HandleNotFoundException("Distributor With ID : " + id + " Is Not Found");
        }
        return userdata.get();
    }

    // create
    public ResponseEntity<ResponseData<User>> insert(User user, Errors errors) {
        ResponseData<User> response = new ResponseData<>();
        // validation
        if (errors.hasErrors()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.setMessages(ErrorUtil.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } else if (repoUser.findAllUserId().contains(user.getUserId())) {
            throw new BadRequestException("This User ID has already been taken!");
        } else if (user.getPrincipal().getId() < 1) {
            throw new BadRequestException("Principal Can't Be Empty");
        } else if (repoPrincipal.findDeletedPrincipal(user.getPrincipal().getId()) == true) {
            throw new BadRequestException("Selected Principal Not Found");
        } else if (!repoAdmin.existsByUsername(user.getCreatedBy())
                || !repoAdmin.existsByUsername(user.getUpdateBy())) {
            throw new HandleNotFoundException("Created/Updated By Admin not found!");
        }

        user.setFullName(user.getFullName().toUpperCase());
        repoUser.save(user);

        response.setStatus(HttpStatus.OK.value());
        response.getMessages().add("Successfully Added User!");
        response.setPayload(user);
        return ResponseEntity.ok(response);
    }

    // edit
    public ResponseEntity<ResponseData<User>> update(int id, User user, Errors errors) {
        ResponseData<User> response = new ResponseData<>();
        Optional<User> userData = repoUser.findById(id);
        // validation
        if (!userData.isPresent()) {
            throw new HandleNotFoundException("User With ID : " + id + " Is Not Found");
        } else if (errors.hasErrors()) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.setMessages(ErrorUtil.parseErrors(errors));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        } else if (user.getPrincipal().getId() < 1) {
            throw new BadRequestException("Principal Can't Be Empty");
        } else if (repoPrincipal.findDeletedPrincipal(user.getPrincipal().getId()) == true) {
            throw new BadRequestException("Selected Principal Not Found");
        } else if (!repoAdmin.existsByUsername(user.getCreatedBy())
                || !repoAdmin.existsByUsername(user.getUpdateBy())) {
            throw new HandleNotFoundException("Created/Updated By Admin not found!");
        }
        // prevent from being changed
        user.setRegistrationDate(userData.get().getRegistrationDate());
        user.setCreatedBy(userData.get().getCreatedBy());
        user.setFullName(user.getFullName().toUpperCase());
        repoUser.save(user);

        response.setStatus(HttpStatus.OK.value());
        response.getMessages().add("Successfully Update User!");
        response.setPayload(user);
        return ResponseEntity.ok(response);
    }

    // delete
    public void delete(int id) {
        Optional<User> userData = repoUser.findById(id);
        if (!userData.isPresent()) {
            throw new HandleNotFoundException("User With ID : " + id + " Is Not Found");
        }
        repoUser.deleteById(id);
    }
}
