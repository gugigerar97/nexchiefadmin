package id.co.nexsoft.nexchiefadmin.controllers;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.nexchiefadmin.dto.ResponseData;
import id.co.nexsoft.nexchiefadmin.models.Admin;
import id.co.nexsoft.nexchiefadmin.services.AdminService;
import id.co.nexsoft.nexchiefadmin.utils.Config;

@RestController
@CrossOrigin(origins = Config.url)
@RequestMapping("/api/admin")
public class AdminController {
    @Autowired
    AdminService AdminService;

    // getall
    @GetMapping
    public List<Admin> getAllAdmins() {
        return AdminService.getAll();
    }

    // getbyid
    @GetMapping("/{id}")
    public Admin getAdmin(@PathVariable("id") int id) {
        return AdminService.getAdminById(id);
    }

    // create
    @PostMapping
    public ResponseEntity<ResponseData<Admin>> insertAdmin(@Valid @RequestBody Admin admin, Errors errors) {
        return AdminService.insert(admin, errors);
    }

    // update
    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<Admin>> updateAdmin(@PathVariable("id") int id, @Valid @RequestBody Admin admin,
            Errors errors) {
        return AdminService.update(id, admin, errors);
    }

    // delete
    @DeleteMapping("/{id}")
    public void deleteAdmin(@PathVariable("id") int id) {
        AdminService.delete(id);
    }

    // login
    @PostMapping("/login")
    public Admin loginAdmin(@RequestBody Admin admin) {
        return AdminService.login(admin);
    }
}
